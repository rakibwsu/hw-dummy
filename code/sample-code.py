import argparse


# code copied taken from here
# https://docs.python.org/3/howto/argparse.html

def main(train, test):
    with open(train) as train_file:
        print(train_file.readlines())

    with open(test) as test_file:
        print(test_file.readlines())

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("trainFile")
    parser.add_argument("testFile")
    args = parser.parse_args()
    print(args)

    # print(args)
    main(args.trainFile, args.testFile)