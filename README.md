# HW-Submission-Sample

## Submissions must accompany a README file

Please following the instructions below

* mention which python/JDK version you are using.
* submit in a .zip file. The name of .zip will be WSUID_LastName
* include a script to run the code and it needs to be noted in the README file
* don't submit the datafolder, I will add them in place
* Output needs to be well formatted
* It's encouraged to format the code in a nice structure
* Meaningful Comments can help both of us


# Sample Output Example

```
Train Accuracy 90%
Test Accuracy  85%
Precision      .91
Recall         .92
F1 Score       .90
```
